#! /bin/sh

git submodule init
git submodule update

cat > ChangeLog <<EOT
This file is a placeholder. It will be replaced with the actual ChangeLog
by make.
EOT

for dir in m4 html tmp;
do
    test -d $dir || mkdir $dir
done

autoreconf -f -i -s
