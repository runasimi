BEGIN {
    FS="\t"
    LANG["quechua"]=1
    LANG["tucuman"]=3
    LANG["ayacucho"]=4
    LANG["cochabamba"]=5
    LANG["cozco"]=6
    LANG["imbabura"]=7
    LANG["huaylas"]=8
    LANG["deutsch"]=9
    LANG["english"]=10
    LANG["espanol"]=11

    KEY["deutsch"]=1
    KEY["english"]=1
    KEY["espanol"]=1
	    
    for (lang in LANG) {
	    if (DIR) 
		    filename = DIR "/" lang ".ignore"
	    else
	            filename = lang ".ignore"
	    while ((getline < filename) > 0) {
		    ignore_tab[lang,$1]=1
	    }
    }

}	

function d(s) {
    if (match(s,"^\".*\"$")) 
	s = substr(s,2,length(s)-2);
    gsub("'","\\'",s);
    return s
}	

function ispunct(word) {
	return match(word,"[[:punct:]]")
}

function ignore_word(lang,word) {
	if (word == "" || ispunct(word))
		return 1;
	return ignore_tab[lang,tolower(word)]
}

/^#.*/ {next}
NF==11 {
    key = d($1);
    pos = $2;
    for (lang in LANG) {
	n = split(d($LANG[lang]),a,";");
	for (i=1; i<=n; i++) {
	    match(a[i]," *");
	    if (RSTART)
		s = substr(a[i], RLENGTH+1)
	    else
	        s = a[i]
	    recno++
   	    printf("INSERT INTO %s VALUES(%d,'%s','%s','%s');\n",
	           lang, recno, key, pos, s);

	    if (KEY[lang]) {
		    # Eliminate parenthesized expressions
		    s1=s
		    s=""
		    while (match(s1,"\\(.[^)]*\\)")) {
			    s = s "" substr(s1, 1, RSTART-1)
			    s1 = substr(s1, RSTART+RLENGTH)
		    }
		    s = s "" s1

                    # Create reverse key entries
		    k = split(s,b,"[ \t[:punct:]()]");
		    for (j=1; j<=k; j++) {
			    if (k == 1 || !ignore_word(lang,b[j]))
				    printf("INSERT INTO %s VALUES('%s',%d);\n",
					   lang "_key", b[j], recno)
		    }
	    }
	}
    }	
    next
}

{
    print NR ": " NF " fields" > "/dev/stderr"
}
