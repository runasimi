-- MySQL dump 10.9
--
-- Host: localhost    Database: runasimi
-- ------------------------------------------------------
-- Server version	4.1.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Table structure for table `ayacucho`
--

DROP TABLE IF EXISTS `ayacucho`;
CREATE TABLE `ayacucho` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `cochabamba`
--

DROP TABLE IF EXISTS `cochabamba`;
CREATE TABLE `cochabamba` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `cozco`
--

DROP TABLE IF EXISTS `cozco`;
CREATE TABLE `cozco` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `deutsch`
--

DROP TABLE IF EXISTS `deutsch`;
CREATE TABLE `deutsch` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `deutsch_key`
--

DROP TABLE IF EXISTS `deutsch_key`;
CREATE TABLE `deutsch_key` (
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  `serial` int(10) NOT NULL default '0',
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `english`
--

DROP TABLE IF EXISTS `english`;
CREATE TABLE `english` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `english_key`
--

DROP TABLE IF EXISTS `english_key`;
CREATE TABLE `english_key` (
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  `serial` int(10) NOT NULL default '0',
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `espanol`
--

DROP TABLE IF EXISTS `espanol`;
CREATE TABLE `espanol` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `espanol_key`
--

DROP TABLE IF EXISTS `espanol_key`;
CREATE TABLE `espanol_key` (
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  `serial` int(10) NOT NULL default '0',
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `huaylas`
--

DROP TABLE IF EXISTS `huaylas`;
CREATE TABLE `huaylas` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `imbabura`
--

DROP TABLE IF EXISTS `imbabura`;
CREATE TABLE `imbabura` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `quechua`
--

DROP TABLE IF EXISTS `quechua`;
CREATE TABLE `quechua` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Table structure for table `tucuman`
--

DROP TABLE IF EXISTS `tucuman`;
CREATE TABLE `tucuman` (
  `serial` int(10) NOT NULL default '0',
  `common` varchar(128) collate utf8_general_ci NOT NULL default '',
  `part_of_speech` varchar(10) collate utf8_general_ci NOT NULL default '',
  `entry` varchar(128) collate utf8_general_ci NOT NULL default '',
  UNIQUE KEY `serial` (`serial`),
  KEY `com_pos` (`common`,`part_of_speech`),
  KEY `com` (`common`),
  KEY `ent` (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

