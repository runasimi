changequote([,])
define([INDEX],[[INDEX] [$1] (shift($@))])
define([UNIQUE], [[UNIQUE] [$1] (shift($@))])


define([LANG],[
CREATE TABLE $1 (
        serial int(10) default 0 not null,
	common varchar(128) default '' not null,
	part_of_speech char(10) default '' not null,
	entry varchar(128) default '' not null,
	UNIQUE(serial,serial),
	INDEX(com_pos,common,part_of_speech),
	INDEX(com,common),
	INDEX(ent,entry)
);])

define([KEY],[
CREATE TABLE $1_key (
	entry varchar(128) default '' not null,
	serial int(10) default 0 not null,
	INDEX(ent,entry)
);])

LANG(quechua)
LANG(tucuman)
LANG(ayacucho)
LANG(cochabamba)
LANG(cozco)
LANG(imbabura)
LANG(huaylas)
LANG(espanol)
KEY(espanol)
LANG(english)
KEY(english)
LANG(deutsch)
KEY(deutsch)