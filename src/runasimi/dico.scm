;;;; This file is a part of Quechua Web Dictionary Search Engine
;;;; Copyright (C) 2008 Sergey Poznyakoff
;;;; 
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;

(define-module (runasimi dico)
  #:use-module (guile-user)
  #:use-module (gamma sql)
  #:use-module (runasimi config)
  #:use-module (runasimi dictdb))

(define (sql-error-handler err descr)
  (format #t "cannot connect to the database")
  (with-output-to-port
      (current-error-port)
    (lambda ()
      (display err)
      (display ": ")
      (display descr))))  

(define (dico-error err . rest)
  (with-output-to-port
      (current-error-port)
    (lambda ()
      (display err)
      (for-each
       display
       rest)
      (newline))))

(define (my-sql-query conn query)
  (catch #t
	 (lambda ()
	   (sql-query conn query))
	 (lambda args
	   '())))

;; Dico interface

(define (open-module name . rest)
  (let ((target-language "es"))
    (for-each (lambda (arg)
		(let ((av (string-split arg #\=)))
		  (case (length av)
		    ((2) (let ((var (car av)))
			   (cond
			    ((string=? var "lang")
			     (set! target-language (cadr av)))
			    (else
			     (dico-error "Unknown option " (car av))))))
		    (else
		     (dico-error "Unknown option " (car av))))))
	      rest)
    (let ((conn (sql-connect
		 sql-iface sql-host sql-port sql-database
		 sql-username sql-password)))
      (sql-query conn "SET NAMES utf8")
      (list
       conn
       target-language
       (dictdb:lang-to table (assoc 'qu  lang-list))
       (dictdb:lang-to table (assoc (string->symbol target-language)
				    lang-list))))))

(defmacro dbh:conn (dbh) `(list-ref ,dbh 0))
(defmacro dbh:lang (dbh) `(list-ref ,dbh 1))
(defmacro dbh:srctab (dbh) `(list-ref ,dbh 2))
(defmacro dbh:dsttab (dbh) `(list-ref ,dbh 3))

(define (close-module dbh)
  (sql-connect-close (dbh:conn dbh)))

(define descr-list
  '(("es" . "Diccionário Quechua-Español")
    ("en" . "Quechua-English Dictionary")
    ("de" . "Quechua-Deutsch Wörterbuch")))

(define (descr dbh)
  (let ((res (assoc (dbh:lang dbh) descr-list)))
    (if res
	(cdr res)
	#f)))

(define info-list
  '(("es" .
     "Diccionário Runasimi-Español.\n\
Copyright © 2004, 2005, 2008 Sergey Poznyakoff.\n\
Vocabulario compuesto por Philip Jacobs (http://www.runasimi.de/runaespa.htm).\n\
\n\
Contenido disponible bajo los términos de la Licencia de documentación libre\n\
de GNU (véase http://www.gnu.org/licenses/fdl.html")
    ("en" .
     "Quechua-English dictionary.\n\
Copyright © 2004, 2005, 2008 Sergey Poznyakoff.\n\
Dictionary corpus by Philip Jacobs (http://www.runasimi.de/runaespa.htm).\n\
\n\
Dictionary is available under the terms of the GNU Free Documentation License,\n\
see http://www.gnu.org/licenses/fdl.html")
    ("de" .
     "Quechua-Deutsch Wörterbuch.\n\
Copyright © 2004, 2005, 2008 Sergey Poznyakoff.\n\
Basiert auf dem Werk von Philip Jacobs (http://www.runasimi.de/runaespa.htm).\n\
\n\
Ihr Text steht unter der GNU-Lizenz für freie Dokumentation\n\
(http://www.gnu.org/licenses/fdl.html)")))

(define (info dbh)
  (let ((res (assoc (dbh:lang dbh) info-list)))
    (if res
	(cdr res)
	#f)))


(define (define-word dbh word)
  (let ((res (sql-query (dbh:conn dbh)
			(dictdb:build-forward-query
			 (dbh:srctab dbh)
			 (dbh:dsttab dbh)
			 word))))
    (and res (cons #t (dictdb:compact-forward-reply res)))))

(defmacro rh:define? (res)
  `(car ,res))

(defmacro rh:result (res)
  `(cdr ,res))

(define (match-exact dbh strat word)
  (my-sql-query
   (dbh:conn dbh)
   (dictdb:build-list-query
    (dbh:srctab dbh) (dbh:dsttab dbh)
    (string-append "= \"" word "\""))))

(define (match-prefix dbh strat word)
  (my-sql-query
   (dbh:conn dbh)
   (dictdb:build-list-query
    (dbh:srctab dbh) (dbh:dsttab dbh)
    (string-append "LIKE \"" word "%\""))))

(define (match-suffix dbh strat word)
  (my-sql-query
   (dbh:conn dbh)
   (dictdb:build-list-query
    (dbh:srctab dbh) (dbh:dsttab dbh)
    (string-append "LIKE \"%" word "\""))))

(define (match-extnd-regex dbh strat word)
  (my-sql-query
   (dbh:conn dbh)
   (dictdb:build-list-query
    (dbh:srctab dbh) (dbh:dsttab dbh)
    (string-append "REGEXP \"" word "\""))))

(define (match-basic-regex dbh strat word)
  #f) ; FIXME

;; Convert SLIST, which is a list of strings, into a string of
;; comma-separated values.
(define (list->csv slist)
  (apply string-append
	 (let loop ((elt '())
		    (slist slist))
	   (cond
	    ((null? (cdr slist))
	     (reverse
	      (cons "\"" (cons (car slist) (cons "\"" elt)))))
	    (else
	     (loop (cons "\"," (cons (car slist) (cons "\"" elt)))
		   (cdr slist)))))))

(define (match-selector dbh strat key)
  (let ((dlist (mapcan
		(lambda (elt)
		  (let ((word (car elt)))
		    (and (dico-strat-select? strat word key)
			 word)))
		(my-sql-query
		 (dbh:conn dbh)
		 (dictdb:build-list-query
		  (dbh:srctab dbh) (dbh:dsttab dbh) #f)))))
    (if (not (null? dlist))
	(my-sql-query
	 (dbh:conn dbh)
	 (dictdb:build-list-query
	  (dbh:srctab dbh) (dbh:dsttab dbh)
	  (string-append " IN (" (list->csv dlist) ")")))
	#f)))

(define (match-reverse dbh src-table key)
  (my-sql-query
   (dbh:conn dbh)
   (dictdb:build-reverse-list-query
    (dbh:dsttab dbh) (dbh:srctab dbh) key)))

(define strategy-list
  (list (cons "exact"  match-exact)
	(cons "prefix"  match-prefix)
	(cons "suffix"  match-suffix)
	(cons "re"  match-extnd-regex)
	(cons "rev-qu" match-reverse)))

(define (match-word dbh strat key)
  (let ((sp (assoc (dico-strat-name strat) strategy-list)))
    (let ((res (cond
		(sp
		 ((cdr sp) dbh strat (dico-key->word key)))
		((dico-strat-selector? strat)
		 (match-selector dbh strat key))
		(else
		 (match-prefix dbh strat (dico-key->word key))))))
      (and res (cons #f (map car res))))))

(define (output rh n)
  (let ((res (rh:result rh)))
    (cond
     ((rh:define? rh)
      (dictdb:display-compacted-forward-result res 'text))
     (else
      (display (list-ref res n))))))

(define (result-count rh)
  (length (rh:result rh)))

(define-public (dico-runasimi-init arg)
  (list (cons "open" open-module)
        (cons "close" close-module)
        (cons "descr" descr)
        (cons "info" info)
        (cons "define" define-word)
        (cons "match" match-word)
        (cons "output" output)
        (cons "result-count" result-count)))

;;
;; Setup
(runasimi-config-setup)
(dico-register-strat "suffix" "Match word suffixes")
(dico-register-strat "rev-qu" "Reverse search in Quechua databases")




