;;;; This file is a part of Quechua Web Dictionary Search Engine -*- scheme -*-
;;;; Copyright (C) 2005, 2007, 2008, 2015 Sergey Poznyakoff
;;;; 
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;

(define-module (runasimi dictdb)
  #:use-module (gamma sql)
  #:export-syntax (dictdb:lang-to))

(define-public lang-list
  ;; key  Option    Table    Flag    
  ;;----+---------+---------+------
  '((en  "English" "english" #f)
    (de  "Deutsch" "deutsch" #f)
    (es  "Español" "espanol" #f)
    (qu  "Quechua" "quechua" #t)))

(define-syntax dictdb:lang-to
  (syntax-rules (name table flag)
    ((lang-to name lang)
     (list-ref lang 1))
    ((lang-to table lang)
     (list-ref lang 2))
    ((lang-to flag lang)
     (list-ref lang 3))))

(define-public dialect-list
  ;;    key        Name            Table
  ;; ------------+-------------+--------------
  '(("cozco"        "Cozco"       "cozco")
    ("ayacucho"     "Ayacucho"    "ayacucho")
    ("cochabamba"   "Cochabamba"  "cochabamba")
    ("tucuman"      "Tucumán"     "tucuman")
    ("imbabura"     "Imbabura"    "imbabura")))
  

;; FIXME
(define (regexp? str)
  (or (string-index str #\*)
      (string-index str #\?)))

;;; NOTE: Terms accepted in the function names and comments below:
;;; 1) "POS" = "part of speech"
;;; 2) "SRC" = "source language" is the language to translate from
;;; 3) "DST" = "destination language" is the language to transalte
;;;    to
;;; 4) "forward" = "forward translation" means the translation from
;;;    Quechua to a destination language
;;; 5) "reverse" = "reverse translation" means the translation from
;;;    the given source language to Quechua

(define-public (dictdb:build-list-query src-table dst-table key-expr)
  (string-append
   "SELECT DISTINCT q.common FROM "
   src-table " q, "
   dst-table " e WHERE e.common=q.common "
   (if key-expr
       (string-append "AND q.common " key-expr)
       "")
   " ORDER BY 1"))

(define-public (dictdb:build-forward-query src-table dst-table key)
  (let ((key-expr (string-append (if (regexp? key)
				     " REGEXP "
				     "=")
				 "\"" key "\"")))
    (string-append
     "SELECT q.part_of_speech, q.common, e.entry FROM "
     src-table " q, "
     dst-table " e WHERE e.common=q.common AND e.part_of_speech=q.part_of_speech AND q.common"
     key-expr
     " ORDER BY 2,1")))

(defmacro raw-reply:pos (r)
  `(list-ref ,r 0))
(defmacro raw-reply:word (r)
  `(list-ref ,r 1))
(defmacro raw-reply:trans (r)
  `(list-ref ,r 2))
(defmacro raw-reply:to-header (r)
  `(cons (raw-reply:word ,r) (raw-reply:pos ,r)))

(defmacro reply:pos (r)
  `(cdar ,r))

(defmacro reply:word (r)
  `(caar ,r))

(defmacro reply:trans-list (r)
  `(cdr ,r))

(defmacro reply:nth (r n)
  `(list-ref (reply:trans-list ,r) n))

(define-public (dictdb:compact-forward-reply reply)
  (if (null? reply)
      reply
      (let* ((compacted-reply '())
	     (header (raw-reply:to-header (car reply)))
	     (hstr (string-append (car header) (cdr header)))
	     (cur (list (raw-reply:trans (car reply)))))
	(for-each
	 (lambda (x)
	   (if (string=? hstr (string-append (raw-reply:word x)
					     (raw-reply:pos x)))
	       (set! cur (cons (raw-reply:trans x) cur))
	       (let ((entry (cons header (reverse cur))))
		 (set! compacted-reply (cons entry
					     compacted-reply))
		 (set! header (raw-reply:to-header x))
		 (set! hstr (string-append (raw-reply:word x)
					   (raw-reply:pos x)))
		 (set! cur (list (raw-reply:trans x))))))
	 (cdr reply))
	(reverse (cons (cons header (reverse cur)) compacted-reply)))))

(define-public (dictdb:build-forward-dialect-query src-desc dst-desc key)
  (let ((key-expr (string-append (if (regexp? key)
				     " REGEXP "
				     "=")
				 "\"" key "\"")))
    (string-append
     "SELECT q.part_of_speech, q.entry "
     ", e.entry FROM "
     (caddr src-desc) " q, "
     (caddr dst-desc) " e WHERE e.common=q.common AND e.part_of_speech=q.part_of_speech AND q.entry"
     key-expr
     " ORDER BY 2,1")))

(define-public (dictdb:build-reverse-query src-desc dst-desc key)
  (if (regexp? key)
      (let ((key-expr (string-append " REGEXP "
				     "\"" key "\"")))
	(string-append
	 "SELECT dst.part_of_speech, dst.entry, src.entry FROM "
	 (caddr src-desc) " src, "
	 (caddr dst-desc) " dst WHERE src.common=dst.common \
AND dst.part_of_speech=src.part_of_speech AND src.entry "
	 key-expr " ORDER BY 1"))
      (string-append
       "SELECT dst.part_of_speech, dst.entry, src.entry FROM "
       (caddr src-desc) " src, "
       (caddr src-desc) "_key k,"
       (caddr dst-desc) " dst WHERE src.common=dst.common AND \
k.serial=src.serial AND k.entry = \"" key "\"")))

(define-public (dictdb:build-reverse-list-query src-table dst-table key)
  (string-append
   "SELECT dst.common FROM "
   src-table " src, "
   src-table "_key k, "
   dst-table " dst "
   "WHERE src.common=dst.common "
   "AND k.serial=src.serial "
   "AND k.entry=\"" key "\""))
       
;;; Display

(define out-method-list
  (list
   (cons 'html (list
		 ;; Begin
		 (lambda ()
		   (display "<table>"))
		 ;; End
		 (lambda ()
		   (display "</table>"))
		 ;; print-header
		 (lambda (word pos)
		   (display "<tr><td>")
		   (display "<b>")
		   (display word)
		   (display "</b>, <i>")
		   (display pos)
		   (display "</i></td></tr>"))
		 ;; print-trans
		 (lambda (n trans)
		   (display "<tr><td></td><td>")
		   (display trans)
		   (display "</td></tr>"))))
   (cons 'text (list
		 ;; Begin
		 (lambda () #f)
		 ;; End
		 newline
		 ;; print-header
		 (lambda (word pos)
		   (display word)
		   (display ", <")
		   (display pos)
		   (display ">")
		   (newline))
		 ;; print-trans
		 (lambda (n trans)
		   (display n)
		   (display ". ")
		   (display trans)
		   (display ";\n"))))))

(defmacro out-get-function (x n)
  `(list-ref ,x ,n))

(defmacro out-begin-output (x)
  `(out-get-function ,x 0))

(defmacro out-end-output (x)
  `(out-get-function ,x 1))

(defmacro out-print-header (x)
  `(out-get-function ,x 2))

(defmacro out-print-trans (x)
  `(out-get-function ,x 3))


;; Result format is:
;; (POS SRC DST)

(define-public (dictdb:display-forward-result res method)
  (let ((key "")
	(value "")
	(num 0)
	(out (cdr (assoc method out-method-list))))
    ((out-begin-output out))
    (for-each
     (lambda (x)
       (let ((k (string-append (car x) (cadr x))))
	 (if (not (string=? key k))
	     (begin
	       (set! key k)
	       (set! num 0)
	       (set! value "")
	       ((out-print-header out) (list-ref x 1) (car x))))
	 (if (not (string=? value (list-ref x 2)))
	     (begin
	       ;; Print translation
	       (set! value (list-ref x 2))
	       (set! num (1+ num))
	       ((out-print-trans out) num value)))))
     res)
    ((out-end-output out))))

(define-public (dictdb:display-compacted-forward-result res method)
  (let ((out (cdr (assoc method out-method-list))))
    ((out-begin-output out))
    (for-each
     (lambda (x)
       ((out-print-header out) (reply:word x) (reply:pos x))
       (let ((num 0))
	 (for-each
	  (lambda (trans)
	    (set! num (1+ num))
	    ((out-print-trans out) num trans))
	  (reply:trans-list x))))
     res)))

;; Implementation of a Common LISP mapcan function
(define-public (mapcan fun list)
  (apply (lambda ( . slist)
	   (let loop ((elt '())
		      (slist slist))
	     (cond
	      ((null? slist)
	       (reverse elt))
	      ((not (car slist))
	       (loop elt (cdr slist)))
	      (else
	       (loop (cons (car slist) elt) (cdr slist))))))
	 (map fun list)))

